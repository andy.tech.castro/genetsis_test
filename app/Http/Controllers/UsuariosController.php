<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Provincia;
use App\Preferencia;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    public function usuarios()
    {
        $usuarios =Usuario::with('preferencias')->get();
        foreach ($usuarios as $usuario) {
            $provincia = Provincia::find($usuario->provincia);
            $usuario->id_provincia = $provincia->id_provincia;
            $usuario->provincia = $provincia->provincia;
        }
        return $usuarios;
    }

    public function usuario()
    {
        $provincias = Provincia::all();
        $sexo = ['Hombre', 'Mujer'];
        $preferencias = Preferencia::all();
        return view('nuevo_usuario', [
            'provincias' => $provincias,
            'preferencias' => $preferencias,
            'sexo' => $sexo
        ]);
    }

    public function guardarUsuario(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:100'],
            'apellidos' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:100', 'unique:usuarios'],
            'direccion' => ['nullable','string', 'max:255'],
            'provincia' => ['required', 'integer'],
            'sexo' => ['required', 'string'],
            'preferencias' => ['nullable'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
        }
        $data = $validator->validated();
        $usuario = Usuario::create([
            'nombre' => $data['nombre'],
            'apellidos' => $data['apellidos'],
            'email' => $data['email'],
            'direccion' => $data['direccion'],
            'provincia' => $data['provincia'],
            'sexo' => $data['sexo'],
        ]);

        if (isset($data['preferencias'])) {
            foreach ($data['preferencias'] as $key => $value) {
                $usuario->preferencias()->attach($value);
            }
        }

        return redirect("/")->with('success', 'El Usuario se ha guardado');
    }
}
