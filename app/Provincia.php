<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $primaryKey = 'id_provincia';

    protected $fillable = [
        'id_provincia',
        'provincia'
    ];

    public $timestamps = false;
}
