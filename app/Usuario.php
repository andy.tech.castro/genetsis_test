<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = [
        'nombre', 'apellidos', 'email', 'password', 'direccion', 'preferencias', 'provincia', 'sexo'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function preferencias()
    {
        return $this->belongsToMany(Preferencia::class);
    }
}
