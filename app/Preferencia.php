<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferencia extends Model
{
    protected $primaryKey = 'id_preferencia';

    protected $fillable = [
        'id_preferencia',
        'preferencia'
    ];

    public $timestamps = false;

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class);
    }
}
