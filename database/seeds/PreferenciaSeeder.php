<?php

use Illuminate\Database\Seeder;
use App\Preferencia;

class PreferenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id_preferencia' => 1, 'preferencia' => 'Deportes'],
            ['id_preferencia' => 2, 'preferencia' => 'Cine'],
            ['id_preferencia' => 3, 'preferencia' => 'Motor'],
            ['id_preferencia' => 4, 'preferencia' => 'Informática'],
            ['id_preferencia' => 5, 'preferencia' => 'Cocina'],
            ['id_preferencia' => 6, 'preferencia' => 'Viajes'],
        ];

        foreach ($items as $item) {
            Preferencia::create($item);
        }
    }
}
