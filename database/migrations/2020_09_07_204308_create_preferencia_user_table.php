<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferenciaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferencia_usuario', function (Blueprint $table) {
            //$table->primary('user_id', 'preferencia_id_preferencia');
            $table->id();
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id', 'users_preferencias_usersfk')
                ->references('id')
                ->on('usuarios')
                ->onDelete('cascade');

            $table->smallInteger('preferencia_id_preferencia');
            $table->foreign('preferencia_id_preferencia', 'users_preferencias_preferenciasfk')
                ->references('id_preferencia')
                ->on('preferencias')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferencia_user');
    }
}
